; Include Build Kit install profile makefile via URL
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make

; ====== MODULE EXCLUDES ==================================================
projects[openid] = FALSE
projects[openidadmin] = FALSE

; ====== DEVELOPMENT TOOLS ================================================
; Admin
projects[admin][subdir] = contrib
projects[admin][version] = 2.0-beta3

; Devel
projects[devel][subdir] = contrib
projects[devel][version] = 1.2

; Diff
projects[diff][subdir] = contrib
projects[diff][version] = 2.0

; CTools
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.0

; Features
projects[features][subdir] = contrib
projects[features][version] = 1.0-rc3

; Strongarm
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0-rc1

; Libraries API
projects[libraries][subdir] = contrib
projects[libraries][version] = 2.0-alpha2

; Coder
projects[coder][subdir] = contrib
projects[coder][version] = 1.0

; Grammer (required by Coder)
projects[grammar_parser][subdir] = contrib
projects[grammar_parser][version] = 1.2
projects[grammar_parser_lib][subdir] = contrib
projects[grammar_parser_lib][version] = 1.x-dev

; ====== STRUCTURAL MODULES ================================================
projects[views][subdir] = contrib
projects[views][version] = 3.3

projects[token][subdir] = contrib
projects[token][version] = 1.1
projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.1

; ====== LIBRARIES ================================================

; Profiler library (helper utilities for install profiles)
libraries[profiler][download][type] = file
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz"
